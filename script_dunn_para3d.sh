#!/bin/sh

N=60
np=$1
inew=1
iold=0
folder="Meshes"

make
if ! make ; then
  exit 2
fi

/opt/gmsh/3.0.2/bin/gmsh -3 "$folder/dunn3d.geo" -format mesh
# ./run "DataFiles/data_fissure.txt"
# sed -i 's/mesh_dunn_0/mesh_dunn/g' DataFiles/data_fissure.txt
sleep 2

for i in `seq 0 $N`
do
  echo "Decrochage $i"
  inew=$i
  #sed -i "s/i=$iold/i=$inew/g" Meshes/mesh_dunn_first/mesh_dunn.geo
  #/opt/gmsh/3.0.2/bin/gmsh -2 "$folder/mesh_dunn.geo" -format mesh
  sed -i "s/node $iold/node $inew/g" DataFiles/data_fissure3d.txt
  mpirun -np $1 --mca pml ob1 ./run "DataFiles/data_fissure3d.txt"
  iold=$inew
  echo "iold : $iold, inew : $inew"
  sleep 2
done
