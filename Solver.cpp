#include "Solver.h"
#include "Cholesky"
#include <iostream>

Solver::Solver()
{}


SolverLDLT::SolverLDLT()
{
}

SolverCG::SolverCG()
{}


SolverLDLT::~SolverLDLT()
{
  delete _solverLDLT;
}

SolverCG::~SolverCG()
{
  delete _solverCG;
}


void SolverLDLT::initialise(const Eigen::SparseMatrix<double> A, Eigen::VectorXd b)
{
  _A = A;
  _b = b;
  _solverLDLT = new Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>>();
  _solverLDLT->compute(A);
  _x = _solverLDLT->solve(_b);

}

void SolverCG::initialise(const Eigen::SparseMatrix<double> A, Eigen::VectorXd b)
{
  std::cout << "Dans l'initialisation " << std::endl;
  _A = A;
  _b = b;
  _solverCG = new Eigen::ConjugateGradient<Eigen::SparseMatrix<double>>();
  _solverCG->compute(A);
  _x = _solverCG->solve(_b);
  std::cout << "Fin" << std::endl;
}
