//!\file DataFile.h Fichier qui permet de lire les paramètres du fichier d'entrée
#ifndef H_DATAFILE
#define H_DATAFILE

#include <iostream>
#include <string>
#include <vector>
#include "Types.h"
#include "mpi.h"

//! Classe pour lire le fichier de paramètres.
/*!
  Contient dans ses attributs les paramètres, initialisé avec la méthode readDataFile.

  ###Structure générale du fichier de configuration à rentrer :

  <CODE>
  <STRONG>mesh</STRONG><BR>
  nom_du_fichier_de_maillage<BR>
  <EM>Paramètre indispensable</EM>

  <STRONG>nb_dim</STRONG><BR>
  nombre_de_dimension<BR>
  <EM>Paramètre indispensable</EM><BR>

  <STRONG>elasticity</STRONG><BR>
  valeur_du_module_Young<BR>
  <EM>Valeur par défaut : 210 GPa</EM><BR>

  <STRONG>poisson</STRONG><BR>
  valeur_du_coefficient_Poisson<BR>
  <EM>Valeur par défaut : 0.34</EM><BR>

  <STRONG>thickness</STRONG><BR>
  valeur_epaisseur<BR>
  <EM>Valeur par défaut : 1. mm</EM>

  <STRONG>height</STRONG><BR>
  valeur_hauteur<BR>
  <EM>Paramètre indispensable</EM><BR>

  <STRONG>notch_length</STRONG><BR>
  valeur_longueur_entaille<BR>
  <EM>Valeur par défaut : 0. mm</EM><BR>

  <STRONG>gauss</STRONG><BR>
  regle_de_Gauss<BR>
  <EM>Valeur par défaut : Gauss2d4</EM>
  <EM>Choix parmi Gauss2d4, Gauss2d5, Gauss3d4</EM><BR>

  <STRONG>ref_point</STRONG><BR>
  reference_point valeur_deplacement direction (x=0,y=1,z=2)<BR>
  <EM>Pas de valeur par défaut ; une référence minimum (point ou cote ou face)</EM><BR>

  <STRONG>ref_edge</STRONG><BR>
  reference_cote valeur_deplacement direction (x=0,y=1,z=2)<BR>
  <EM>Pas de valeur par défaut ; une référence minimum (point ou cote ou face)</EM><BR>

  <STRONG>ref_surface</STRONG><BR>
  reference_face valeur_deplacement direction (x=0,y=1,z=2)<BR>
  <EM>Pas de valeur par défaut ; une référence minimum (point ou cote ou face)</EM><BR>

  <STRONG>msh_ep</STRONG><BR>
  valeur_epaisseur_maillage_sur_arete<BR>
  <EM>Paramètre indispensable</EM><BR>

  <STRONG>disconnectedNodes</STRONG><BR>
  node nombre_de_noeuds_a_deconnecter<BR>
  <EM>Valeur par défaut : 0</EM><BR>
  <EM>"node" indispensable</EM><BR>

  <STRONG>solver</STRONG><BR>
  choix_du_solveur<BR>
  <EM>Valeur par défaut : cholesky</EM><BR>
  <EM>Choix parmi cholesky, conjugateGradient</EM><BR>

  <STRONG>result_folder</STRONG><BR>
  nom_du_dossier_de_resultats<BR>
  <EM>Valeur par défaut : results/</EM><BR>
  <EM>Nom doit terminer par "/"</EM><BR>
  </CODE>

*/

class DataFile
{
private:
  std::string _fileName;
  std::string _resultFolder;

  //Maillage et caractéristiques
  std::string _meshName;
  int _nb_dim;
  std::vector<MeshReference> _meshRefs;
  double _meshEp;
  int _disconnectedNodes;

  //Caractéristiques du matériau et déplacement imposé
  double _E, _nu, _b, _a, _h;

  //Calcul numérique et solveur
  // std::string _gauss;
  Gauss _gauss;
  SOLVER _solver;

  //Booléens pour ce qui a été donné dans le data.txt
  bool _if_meshName;
  bool _if_nb_dim;
  bool _if_refEdge;
  bool _if_refPoint;
  bool _if_refTri;
  bool _if_E;
  bool _if_nu;
  bool _if_b;
  bool _if_a;
  bool _if_h;
  bool _if_gauss;
  bool _if_solver;
  bool _if_result_folder;
  bool _if_meshEp;
  bool _if_disconnectedNodes;

public:
  //!Constructeur
  /*!
    \param fileName Nom du fichier de configuration.
  */
  DataFile(std::string fileName);
  //!Méthode pour lire le fichier de configuration
  /*!
    Initialise les valeurs des paramètres à partir des données du fichier.
  */
  void readDataFile();
  //!Accesseur au nom du maillage
  std::string getMeshName(){return _meshName;};
  //!Accesseur au nombre de dimensions
  int getnb_dim(){return _nb_dim;};
  //!Accesseur au module d'Young
  double getE(){return _E;};
  //!Accesseur au coefficient de Poisson
  double getNu(){return _nu;};
  //!Accesseur à la hauteur de la géométrie
  double getH(){return _h;};
  //!Accesseur à la longueur de l'entaille
  double getA(){return _a;};
  //!Accesseur à l'épaisseur de l'éprouvette
  double getB(){return _b;};
  //!Accesseur à la règle de Gauss choisie
  Gauss getGauss(){return _gauss;};
  //!Accesseur au solveur linéaire
  SOLVER getSolver(){return _solver;};
  //!Accesseur aux références des points, arêtes et triangles à déplacements imposés
  std::vector<MeshReference> getMeshReferences(){return _meshRefs;};
  //!Accesseur au nom du fichier de résultats
  std::string getResultFolder(){return _resultFolder;};
  //!Accesseur à l'épaisseur du maillage
  double getMeshEp(){return _meshEp;};
  //!Accesseur au nombre de noeuds à déconnecter
  int getDisconnectedNodes(){return _disconnectedNodes;};
};

#endif
