#include "Tools.h"

std::pair<int,int> Charge(int me, int nb_proc, int nb_op)
{
  int r,q;
  int i1, iN;

  r = nb_op%nb_proc;
  q = nb_op/nb_proc;

  if (r == 0)
  {
    i1 = me*q;
    iN = (me+1)*q;
  }
  else
  {
    if (me < r)
    {
      i1 = me*(q+1);
      iN = (me+1)*(q+1);
    }
    else
    {
      i1 = r*(q+1) + (me - r)*q;
      iN = r*(q+1) + (me - r +1)*q;
    }
  }
  return std::make_pair(i1,iN);
}
