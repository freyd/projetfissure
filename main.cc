//!\file main.cc fichier principal qui appelle les différentes fonctions
#include "FiniteElements.h"
#include "Mesh.h"
#include "DataFile.h"
#include <string>
#include "Solver.h"

int main(int argc, char *argv[])
{
  std::string data_file_name;
  if (argc < 2)
  {
    std::cout << "Please enter the name of your DataFile." << std::endl;
    std::cin >> data_file_name;
  }
  else
  {
    data_file_name = argv[1];
  }

  std::string meshName;
  Eigen::MatrixXd A;
  Eigen::VectorXd b,x;

  int me, Np; //numero et nombre de processeurs
  double start,start3,end3,end,deltat,deltam; //mesure du temps

  MPI_Init(&argc,&argv);

  DataFile* data_file = new DataFile(data_file_name);
  Mesh* mesh(0);
  FiniteElements* finiteEl(0);
  Solver* solver(0);

  MPI_Comm_size(MPI_COMM_WORLD,&Np);
  MPI_Comm_rank(MPI_COMM_WORLD,&me);

  start3 = MPI_Wtime();

  data_file->readDataFile();

  if(me==0) std::cout << "Initialisation..." << std::endl;
  int nb_dimension;
  nb_dimension=data_file ->getnb_dim();
  switch (nb_dimension)
  {
    case 2:
    if(me==0) std::cout << "2D-Problem" << std::endl;
    meshName = data_file->getMeshName();
    mesh = new Mesh2D(data_file);
    mesh->ReadMesh(meshName);
    finiteEl = new Elements2D(mesh, data_file);
    break;

    case 3:
    if(me==0) std::cout << "3D-Problem" << std::endl;
    //  std::cout << "le programme n'est pas encore implémenté, arrêt"<< std::endl;
    //  exit(0);
    meshName = data_file->getMeshName();
    mesh = new Mesh3D(data_file);
    mesh->ReadMesh(meshName);
    finiteEl = new Elements3D(mesh, data_file);
    break;

    default:
    if(me==0) std::cerr << "The number of dimension given is not possible, program stopping" << std::endl;
    exit(0);
    break;
  }

  //Calcul
  if(me==0) std::cout << "Building Stiffness matrix..." << std::endl;
  start = MPI_Wtime();
  finiteEl->buildStiffnessMatrix();
  end = MPI_Wtime();
  deltat = end-start;
  MPI_Allreduce(&deltat,&deltam,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  if (me == 0)
  {
    std::cout << "Solving system..." << std::endl;
    auto start2 = MPI_Wtime();
    finiteEl->SolveSystem();
    auto end2 = MPI_Wtime();
    std::cout << "Solving system took " << end2 - start2 << " seconds" << std::endl;
    std::cout << "Building Stress field..." << std::endl;
    finiteEl->buildStressField();
    std::cout << "Calculating Energy..." << std::endl;
    finiteEl->Energy();

    //Ecriture des solutions : déplacement x, déplacement y , contraintes x, y xy
    //Arguments : 1 booléen, 1 entier
    //false=déplacement, true=Contraintes
    //0=x, 1=y, 2=xy
    std::cout << "Writing solutions..." << std::endl;
    for(int i=0;i<nb_dimension;i++)
    {
      finiteEl-> WriteSolutions(false,i);
    }
    for(int i=0;i<(nb_dimension-1)*3-1;i++)
    {
      finiteEl-> WriteSolutions(true,i);
    }
    finiteEl-> WriteSolutions(true,nb_dimension*(nb_dimension-1)-(nb_dimension-2));
    std::cout << "Writing stress field..." << std::endl;
    finiteEl->WriteStressField();
    std::cout << "Writing energy..." << std::endl;
    finiteEl->WriteEnergy();
    end3 = MPI_Wtime();
    std::cout << "End of program." << std::endl;
    std::cout << "--------------------------------------------------" << std::endl;
    std::cout << "Building Stiffness matrix and forces took " << deltam << " seconds." << std::endl;
    std::cout << "Building Stiffness matrix and forces took " << deltam/(end3-start3)*100. << "% of the program duration." << std::endl;
    std::cout << "Solving the system took " << (end2-start2)/(end3-start3)*100. << "% of the program duration." << std::endl;
    std::cout << "--------------------------------------------------" << std::endl;
  }
  delete data_file;
  delete solver;
  delete finiteEl;

  MPI_Finalize();

  return 0;
}
