#include "DataFile.h"
#include <fstream>

//Constructeur
DataFile::DataFile(std::string fileName):
_fileName(fileName), _if_meshName(false), _if_refEdge(false), _if_refPoint(false),
_if_E(false), _if_nu(false), _if_h(false), _if_gauss(false),
_if_solver(false), _if_meshEp(false), _if_disconnectedNodes(false),
_if_a(false), _if_b(false), _if_nb_dim(false), _if_refTri(false)
{}


//Lecture du fichier de données
void DataFile::readDataFile()
{
  std::ifstream data_file(_fileName.data());
  int me, Np;

  MPI_Comm_size(MPI_COMM_WORLD,&Np);
  MPI_Comm_rank(MPI_COMM_WORLD,&me);

  if (!data_file.is_open())
  {
    if (me ==0) std::cout << "Unable to open file " << _fileName << std::endl;
    abort();
  }
  if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
  if (me ==0) std::cout << "Reading data file " << _fileName << std::endl;

  std::string file_line;
  MeshReference meshRef;
  int ref;
  bool isEdge, direction;
  double value;
  std::string tmp, gauss, solv;
  std::map<std::string,Gauss> mapStringGauss;
  std::map<std::string,SOLVER> mapStringSOLVER;

  // Initialisation de la map
  mapStringGauss["Gauss2d4"] = Gauss2d4;
  mapStringGauss["Gauss2d5"] = Gauss2d5;
  mapStringGauss["Gauss3d4"] = Gauss3d4;
  mapStringGauss["Gauss3d5"] = Gauss3d5;
  mapStringGauss["Gauss3d6"] = Gauss3d6;

  mapStringSOLVER["cholesky"] = solvLDLT;
  mapStringSOLVER["conjugateGradient"] = solvCG;

  while (!data_file.eof())
  {
    getline(data_file,file_line);
    if (file_line.find("mesh") != std::string::npos)
    {
      data_file >> _meshName;
      _if_meshName = true;
    }

    if (file_line.find("nb_dim") != std::string::npos)
    {
      data_file >> _nb_dim;
      _if_nb_dim = true;
    }

    if (file_line.find("elasticity") != std::string::npos)
    {
      data_file >> _E;
      _E *= 1.e+3;
      _if_E = true;
    }

    if (file_line.find("poisson") != std::string::npos)
    {
      data_file >> _nu;
      _if_nu = true;
    }

    if (file_line.find("thickness") != std::string::npos)
    {
      // std::cout << "h" << std::endl;
      data_file >> _b;
      _if_b = true;
    }

    if (file_line.find("notch_length") != std::string::npos)
    {
      data_file >> _a;
      _if_a = true;
    }

    if (file_line.find("height") != std::string::npos)
    {
      data_file >> _h;
      _if_h = true;
    }

    if (file_line.find("gauss") != std::string::npos)
    {
      data_file >> gauss;
      // Message d'erreur dans le cas ou un mauvais set est rentré
      _gauss = mapStringGauss[gauss];
      _if_gauss = true;
      // if ((_gauss != "gauss2d4") && (_gauss != "gauss2d5"))
      // {
      //   std::cout << "Only gauss2d4 and gauss2d5 are implemented." << std::endl;
      //   exit(0);
      // }
    }

    if (file_line.find("ref_edge") != std::string::npos)
    {
      data_file >> meshRef.ref; //La référence
      data_file >> meshRef.value; //Valeur numérique
      data_file >> meshRef.direction;
      meshRef.dim = 1;
      _meshRefs.push_back(meshRef);
      _if_refEdge = true;
    }

    if (file_line.find("ref_point") != std::string::npos)
    {
      data_file >> meshRef.ref;
      data_file >> meshRef.value;
      data_file >> meshRef.direction;
      meshRef.dim = 0;
      _meshRefs.push_back(meshRef);
      _if_refPoint = true;
    }

    if (file_line.find("ref_surface") != std::string::npos)
    {
      data_file >> meshRef.ref;
      data_file >> meshRef.value;
      data_file >> meshRef.direction;
      meshRef.dim = 2;
      _meshRefs.push_back(meshRef);
      _if_refTri = true;
    }

    if (file_line.find("disconnectedNodes") != std::string::npos)
    {
      data_file >> tmp;
      data_file >> _disconnectedNodes;
      _if_disconnectedNodes = true;
    }

    if (file_line.find("msh_ep") != std::string::npos)
    {
      data_file >> _meshEp;
      _if_meshEp = true;
    }

    if (file_line.find("solver") != std::string::npos)
    {
      data_file >> solv;
      _solver = mapStringSOLVER[solv];
      _if_solver = true;
    }

    if (file_line.find("result_folder") != std::string::npos)
    {
      data_file >> _resultFolder;
      _if_result_folder = true;
    }
  }

  if (!_if_meshName)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Do not forget to give the mesh name in the data file." << std::endl;
    exit(0);
  }

  if (!_if_nb_dim)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Do not forget to give the dimension of the problem." << std::endl;
    exit(0);
  }


  if (!_if_refEdge && !_if_refPoint && !_if_refTri)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Do not forget to give the references of the constraindes nodes in the data file." << std::endl;
    exit(0);
  }

  if (!_if_meshEp)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Do not forget to give the mesh thickness." << std::endl;
    exit(0);
  }

  if (!_if_h)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Do not forget to give the height of the material." << std::endl;
    exit(0);
  }

  if (!_if_disconnectedNodes)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Beware - The default value (0) is used for the number of disconnected nodes." << std::endl;
    _disconnectedNodes = 0;
  }

  if (!_if_E)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Beware - The default value (210 GPa) is used for the elasticity module." << std::endl;
    _E = 210.e+3; //Choisir l'unité de E !!!!!!!!
  }

  if (!_if_nu)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Beware - The default value (0.34) is used for the Poisson coef." << std::endl;
    _nu = 0.34;
  }

  switch(_nb_dim)
  {
    case 2:
      if (!_if_b)
      {
        if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
        if (me ==0) std::cout << "Beware - The default value (1. mm) is used for the thickness." << std::endl;
        _b = 1.; //Choix de l'unité !!!!!
      }
      break;
    case 3 :
      _b=1.;
      break;
  }

  if (!_if_a)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Beware - The default value (0.) is used for the length of the notch." << std::endl;
    _a = 0.;
  }

  if (!_if_gauss)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Beware - The default value (gauss2d4) is used for the gauss rule." << std::endl;
    _gauss = Gauss2d4;
  }

  if (!_if_solver)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Beware - The default value (Cholesky) is used for the solver." << std::endl;
    // _solver = "Cholesky";
    _solver = solvLDLT;
  }

  if (!_if_result_folder)
  {
    if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
    if (me ==0) std::cout << "Beware - The default folder name (results) is used for the result folder." << std::endl;
    _resultFolder = "results/";
  }

  if (me ==0) std::cout << "End of reading" << std::endl;
  if (me ==0) std::cout << "--------------------------------------------------" << std::endl;
}
