#include "FiniteElements.h"
#include "Solver.h"
#include <fstream>
#include <vector>
#include "Sparse"
#include "Tools.h"

//Constructeur
FiniteElements::FiniteElements(Mesh* mesh, DataFile* data_file)
{
  _mesh = mesh;
  _nb_dim = data_file ->getnb_dim();

  _h = data_file->getH();
  _E = data_file->getE();
  _nu = data_file->getNu();
  _a = data_file->getA();
  _b = data_file->getB();
  buildGaussSet(data_file->getGauss());
  _disconnectedNodes = data_file->getDisconnectedNodes();
  _meshEp = data_file->getMeshEp();
  _crackLength = _meshEp*_disconnectedNodes;
  if (_crackLength > _h-_a)
  {
    std::cout << "Error in the number of disconnected nodes given. Program stops." << std::endl;
    exit(0);
  }
  SOLVER solv = data_file->getSolver();
  switch (solv) {
    case solvLDLT:
      _solver = new SolverLDLT();
      break;
    case solvCG:
      _solver = new SolverCG();
      break;
    default:
      _solver = new SolverLDLT();
  }

  _resultFolder = data_file->getResultFolder();
  system(("mkdir -p ./" + _resultFolder).c_str());

  computeGeometryQuantities();
  _elastMat.resize((_nb_dim-1)*3,(_nb_dim-1)*3);
  }

//Construction de la matrice d'élasticité D
  void Elements2D::BuildElasticityMatrix()
  {
    _elastMat(0,0) = 1.;
    _elastMat(0,1) = _nu;
    _elastMat(0,2) = 0.;
    _elastMat(1,0) = _nu;
    _elastMat(1,1) = 1.;
    _elastMat(1,2) = 0.;
    _elastMat(2,0) = 0.;
    _elastMat(2,1) = 0.;
    _elastMat(2,2) = (1.-_nu)/2.;
    _elastMat = _E/(1.-_nu*_nu)*_elastMat;
  }

  void Elements3D::BuildElasticityMatrix()
  {
    for(int i=0; i< _elastMat.rows(); i++)
    {
      for(int j=0;j< _elastMat.cols(); j++)
      {
        _elastMat(i,j)=0.;
      }
    }
    _elastMat(0,0)=1.-_nu;
    _elastMat(0,1)=_nu;
    _elastMat(0,2)=_nu;
    _elastMat(1,0)=_nu;
    _elastMat(1,1)=1.-_nu;
    _elastMat(1,2)=_nu;
    _elastMat(2,0)=_nu;
    _elastMat(2,1)=_nu;
    _elastMat(2,2)=1.-_nu;
    _elastMat(3,3)=(1.-2.*_nu)/2.;
    _elastMat(4,4)=(1.-2.*_nu)/2.;
    _elastMat(5,5)=(1.-2.*_nu)/2.;

    _elastMat*=_E/((1.+_nu)*(1.-2.*_nu));

  }

//Destructeur
FiniteElements::~FiniteElements()
{
  delete _mesh;
}


//Récupération des données du maillage
void FiniteElements::computeGeometryQuantities()
{
  _elements = _mesh->GetElements();
  _nb_elements = _elements.size();
  _elem_center = _mesh->GetElementCenter();

  _triangles = _mesh->GetTriangles();
  _nb_tri = _triangles.size();

  _vertices = _mesh->GetVertices();
  _nb_vert = _vertices.size();

  _edges = _mesh->GetEdges();
  _nb_edg = _edges.size();

  _imposeDisplacement = _mesh->GetImposeDisplacement();
  _nb_impDisp = _imposeDisplacement.size();

  _stiffMat.resize(_nb_dim*_nb_vert,_nb_dim*_nb_vert);
  _stiffMat.setZero();
  _oldStiffMat.resize(_nb_dim*_nb_vert,_nb_dim*_nb_vert);
  _oldStiffMat.setZero();
}


//Construction du GaussSet
void FiniteElements::buildGaussSet(Gauss gaussChoice)
{
  std::vector<std::tuple<double,double,double>> gpoints;
  std::vector<double> gweights;
  gpoints.resize(0); gweights.resize(0);

  switch (gaussChoice)
  {
    case Gauss2d4:
    gpoints.push_back(std::make_tuple(0.333333333333333,0.333333333333333,0.));
    gweights.push_back(0.5);
    break;

    case Gauss2d5:
    gpoints.push_back(std::make_tuple(0.166666666666667,0.166666666666667,0.));
    gpoints.push_back(std::make_tuple(0.666666666666667,0.166666666666667,0.));
    gpoints.push_back(std::make_tuple(0.166666666666667,0.666666666666667,0.));
    gweights = std::vector<double>({0.166666666666666,0.166666666666666,0.166666666666666});
    break;

    case Gauss3d4:
    gpoints.push_back(std::make_tuple(0.25,0.25,0.25));
    gweights.push_back(0.166666666666667);
    break;

    default:
    gpoints.push_back(std::make_tuple(0.333333333333333,0.333333333333333,0.));
    gweights.push_back(0.5);
    break;
  }
  _gaussSet.points = gpoints;
  _gaussSet.weights = gweights;
}

//coordonnées des sommets
Eigen::MatrixXd FiniteElements::coordinates(int nb_elem)
{
  int node;
  Eigen::MatrixXd coord;
  coord.resize(_nb_dim+1,_nb_dim); //(3,2) en 2D, (4,3) en 3D
  for (int j=0; j<_nb_dim+1; j++)
  {
    node = _elements[nb_elem].GetElement()(j);
    for (int k=0;k<_nb_dim; k++)
    {
      coord(j,k) = _vertices[node-1].GetVertice()(k); //On décale d'un cran car la table commence à 0 et pas à 1
    }
  }
  return coord;
}

// Dérivées des fonctions d'interpolation en 2D
Eigen::MatrixXd Elements2D::interpolationFunctions(std::tuple<double,double,double> point)
{
  Eigen::MatrixXd T;
  T.resize(2,3);
  T(0,0) = -1.;
  T(1,0) = -1.;
  T(0,1) = 1.;
  T(1,1) = 0.;
  T(0,2) = 0.;
  T(1,2) = 1.;
  return T;
}

// Dérivées des fonctions d'interpolation en 3D
Eigen::MatrixXd Elements3D::interpolationFunctions(std::tuple<double,double,double> point)
{
  Eigen::MatrixXd T;
  T.resize(3,4);
  T(0,0) = 0.;
  T(1,0) = 1.;
  T(2,0) = 0.;
  T(0,1) = 0.;
  T(1,1) = 0.;
  T(2,1) = 1.;
  T(0,2) = -1.;
  T(1,2) = -1.;
  T(2,2) = -1.;
  T(0,3) = 1.;
  T(1,3) = 0.;
  T(2,3) = 0.;
  return T;
}

//Calcul de la jacobienne en 2D
Jacobian Elements2D::jacobian(Eigen::MatrixXd deriv, Eigen::MatrixXd coord)
{
  Eigen::MatrixXd Ji, Jii;
  Jii.resize(2,2);
  double detJ;
  Jacobian jacob;
  assert((deriv.rows() == coord.cols()) && "Problem in matrix size");
  Ji = deriv*coord;
  //Calcul du déterminant de Ji
  detJ = Ji(0,0)*Ji(1,1) - Ji(0,1)*Ji(1,0);
  if (std::abs(detJ) < 1.e-6)
  {
    std::cout << "The determinant is equal to zero." << std::endl;
    exit(0);
  }
  //Calcul de l'inverse de Ji
  Jii(0,0) = Ji(1,1);
  Jii(1,1) = Ji(0,0);
  Jii(1,0) = -Ji(1,0);
  Jii(0,1) = -Ji(0,1);
  Jii = 1./detJ*Jii;
  jacob.matrix = Ji;
  jacob.det = detJ;
  jacob.inverse = Jii;
  return jacob;
}

//Calcul de la jacobienne en 3D
Jacobian Elements3D::jacobian(Eigen::MatrixXd deriv, Eigen::MatrixXd coord)
{
  Eigen::MatrixXd Ji, Jii;
  Jii.resize(3,3);
  double detJ;
  Jacobian jacob;
  assert((deriv.rows() == coord.cols()) && "Problem in matrix size");
  Ji = deriv*coord;
  //Calcul du déterminant de Ji
  detJ = Ji(0,0)*(Ji(1,1)*Ji(2,2)-Ji(2,1)*Ji(1,2))
  - Ji(1,0)*(Ji(0,1)*Ji(2,2)-Ji(2,1)*Ji(0,2))
  +Ji(2,0)*(Ji(0,1)*Ji(1,2)-Ji(1,1)*Ji(0,2));
  if (std::abs(detJ) < 1.e-12)
  {
    std::cout << "The determinant is equal to zero." << std::endl;
    exit(0);
  }
  //Calcul de l'inverse de Ji
  Jii=Ji.inverse();

  jacob.matrix = Ji;
  jacob.det = detJ;
  jacob.inverse = Jii;
  return jacob;
}

//Redispostion des termes de Ti dans Bi en 2D
  Eigen::MatrixXd Elements2D::buildBi(Eigen::MatrixXd Ti)
{
  Eigen::MatrixXd Bi;
  Bi.resize(3,6);
  Bi(0,0) = Ti(0,0);
  Bi(0,1) = 0.;
  Bi(0,2) = Ti(0,1);
  Bi(0,3) = 0.;
  Bi(0,4) = Ti(0,2);
  Bi(0,5) = 0.;

  Bi(1,0) = 0.;
  Bi(1,1) = Ti(1,0);
  Bi(1,2) = 0.;
  Bi(1,3) = Ti(1,1);
  Bi(1,4) = 0.;
  Bi(1,5) = Ti(1,2);

  Bi(2,0) = Ti(1,0);
  Bi(2,1) = Ti(0,0);
  Bi(2,2) = Ti(1,1);
  Bi(2,3) = Ti(0,1);
  Bi(2,4) = Ti(1,2);
  Bi(2,5) = Ti(0,2);
  return Bi;
}

//Redispostion des termes de Ti dans Bi en 3D
  Eigen::MatrixXd Elements3D::buildBi(Eigen::MatrixXd Ti)
{
  Eigen::MatrixXd Bi;
  Bi.resize(6,12);
  for(int i=0; i< Bi.rows(); i++)
    {
      for(int j=0; j< Bi.cols(); j++)
      {
        Bi(i,j)=0.;
      }
    }
  for(int i=0; i<4; i++)
  {
    Bi(0,3*i)=Ti(0,i);
    Bi(1,3*i+1)=Ti(1,i);
    Bi(2,3*i+2)=Ti(2,i);
    Bi(3,3*i+1)=Ti(2,i);
    Bi(3,3*i+2)=Ti(1,i);
    Bi(4,3*i)=Ti(2,i);
    Bi(4,3*i+2)=Ti(0,i);
    Bi(5,3*i)=Ti(1,i);
    Bi(5,3*i+1)=Ti(0,i);
  }
  return Bi;
}


//Matrice de rigidité élémentaire
Eigen::MatrixXd FiniteElements::buildElemStiffnessMatrix(int nb_elem)
{
  //nb_elem : numéro de l'élément (et pas nombre d'éléments !)
  int npg = _gaussSet.points.size();
  Eigen::MatrixXd deriv, coord, elemStiffMat, Ji, Jii, Ti, Bi, Ci, tBiCi;
  //Jii = inverse de Ji
  Jacobian jacob;
  double detJ;
  double weight;  Jii.resize(_nb_dim,_nb_dim);
  elemStiffMat.resize(_nb_dim*(_nb_dim+1),_nb_dim*(_nb_dim+1));
//  Jii.resize(_nb_dim,_nb_dim);
  Bi.resize(_nb_dim*3-3, _nb_dim*(_nb_dim+1));
  Ci.resize(3,3);
  coord = coordinates(nb_elem);
  for (int i=0; i<_nb_dim*(_nb_dim+1); i++)
  {
    for (int j=0; j<_nb_dim*(_nb_dim+1); j++)
    {
      elemStiffMat(i,j) = 0.;
    }
  }
  for (int i=0; i<npg; i++)
  {
    //Calcul de Ji
    deriv = interpolationFunctions(_gaussSet.points[i]);
    jacob = jacobian(deriv,coord);
    Ji = jacob.matrix;
    detJ = jacob.det;
    Jii = jacob.inverse;

    //Calcul de Ti
    Ti = Jii*deriv;
    //Redisposition dans Bi
   Bi=buildBi(Ti);
    weight = _gaussSet.weights[i];
    Ci = weight*detJ*_elastMat;
    tBiCi = Bi.transpose()*Ci;
    elemStiffMat += tBiCi*Bi;
  }

  elemStiffMat = _b*elemStiffMat;
  return elemStiffMat;
}


//Matrice de rigidité
void FiniteElements::buildStiffnessMatrix()
{
  // Déclaration des variables
  Eigen::MatrixXd elemStiffMat;
  Eigen::SparseMatrix<double> tmpStiffMat(_stiffMat.rows(), _stiffMat.cols());
  Eigen::SparseMatrix<double> procStiffMats;
  int procRows, procCols, procNnz;
  int *procInnerIndexPtr, *procOuterIndexPtr;
  double* procValuePtr;
  int rows,cols,nnz;
  int *innerIndexPtr, *outerIndexPtr;
  double *valuePtr;
  std::vector<Eigen::Triplet<double>> triplets;
  int ti, tj, p(0), q(0), me, Np;
  std::pair<int,int> indices;
  MPI_Status stat;

  MPI_Comm_size(MPI_COMM_WORLD,&Np);
  MPI_Comm_rank(MPI_COMM_WORLD,&me);
  indices = Charge(me, Np, _nb_elements);

  //Resize
  procStiffMats.resize(_stiffMat.rows(), _stiffMat.cols());
  procStiffMats.setZero();

  // Affichage du pourcentage par le proc 0
  if (me == 0)
  {
    std::cout << p << " %" << std::endl;
  }

  // Boucle sur les éléments
  // for (int elem=0; elem<_nb_tri; elem++)
  for (int elem=indices.first; elem<indices.second; elem++)
  {
    // Affichage
    if (me == 0)
    {
      q = floor((elem+1)*100./indices.second);
      if (q != p)
      {
        p = q;
        std::cout << p << " %" << std::endl;
      }
    }

    //Matrice élémentaire
    elemStiffMat = buildElemStiffnessMatrix(elem);

    //Assemblage en creux
    triplets.clear();
    tmpStiffMat.setZero();
    for (int i=0; i<_nb_dim+1; i++)
    {
      for (int j=0; j<_nb_dim+1; j++)
      {
        ti = _elements[elem].GetElement()(i)-1; //On décale d'un cran (même raison qu'au dessus)
        tj = _elements[elem].GetElement()(j)-1;
        for (int k=0; k<_nb_dim; k++)
        {
          for (int l=0;l<_nb_dim; l++)
          {
            triplets.push_back({_nb_dim*ti+k,_nb_dim*tj+l,elemStiffMat(_nb_dim*i+k,_nb_dim*j+l)});
          }
        }
	    }
    }
	  tmpStiffMat.setFromTriplets(triplets.begin(), triplets.end());
    procStiffMats+=tmpStiffMat;
  }

  // Communications
  if (me != 0) //Si on n'est pas le proc 0, on envoie
  {
    std::cout << "Sending Stiffness matrix... (" << me << "/" << Np-1 << ")" << std::endl;

    //On récupère les informations
    procStiffMats.makeCompressed();
    rows = procStiffMats.rows();
    cols = procStiffMats.cols();
    nnz = procStiffMats.nonZeros();
    innerIndexPtr = procStiffMats.innerIndexPtr();
    outerIndexPtr = procStiffMats.outerIndexPtr();
    valuePtr = procStiffMats.valuePtr();

    // On envoie
    MPI_Send(&rows,1,MPI_INT,0,100+me,MPI_COMM_WORLD);
    MPI_Send(&cols,1,MPI_INT,0,110+me,MPI_COMM_WORLD);
    MPI_Send(&nnz,1,MPI_INT,0,120+me,MPI_COMM_WORLD);
    MPI_Send(innerIndexPtr,nnz,MPI_INT,0,130+me,MPI_COMM_WORLD);
    MPI_Send(outerIndexPtr,cols+1,MPI_INT,0,140+me,MPI_COMM_WORLD);
    MPI_Send(valuePtr,nnz,MPI_DOUBLE,0,150+me,MPI_COMM_WORLD);
  }
  else
  {
    std::cout << "Receiving Stiffness matrix..." << std::endl;
    _stiffMat += procStiffMats;
    // On recoit une matrice de chaque proc (sauf 0)
    for (int i=1; i<Np; i++)
    {
      // Reception
      MPI_Recv(&procRows,1,MPI_INT,i,100+i,MPI_COMM_WORLD,&stat);
      MPI_Recv(&procCols,1,MPI_INT,i,110+i,MPI_COMM_WORLD,&stat);
      MPI_Recv(&procNnz,1,MPI_INT,i,120+i,MPI_COMM_WORLD,&stat);

      // Initialisation des pointeurs
      procOuterIndexPtr = new int[procCols+1];
      procInnerIndexPtr = new int[procNnz];
      procValuePtr = new double[procNnz];
      // Reception
      MPI_Recv(procInnerIndexPtr,procNnz,MPI_INT,i,130+i,MPI_COMM_WORLD,&stat);
      MPI_Recv(procOuterIndexPtr,procCols+1,MPI_INT,i,140+i,MPI_COMM_WORLD,&stat);
      MPI_Recv(procValuePtr,procNnz,MPI_DOUBLE,i,150+i,MPI_COMM_WORLD,&stat);
      tmpStiffMat = Eigen::Map<Eigen::SparseMatrix<double>> (procRows,procCols,procNnz,procOuterIndexPtr,procInnerIndexPtr,procValuePtr);
      _stiffMat += tmpStiffMat;
    }
    _oldStiffMat = _stiffMat; //on stocke la matrice avant modifications

    std::cout << "Building external forces..."<< std::endl;
    buildExtForces();
    // On supprime les pointeurs qui ont utilisé new
    if (Np > 1)
    {
      delete procOuterIndexPtr, procInnerIndexPtr, procValuePtr;
    }
  }
}

//Vecteur F des forces extérieures
void FiniteElements::buildExtForces()
{
  int node, inode;
  _extForces.resize(_nb_dim*_nb_vert);
  _stiffMat.makeCompressed();
  for(int i=0;i<_nb_dim*_nb_vert;i++)
  {
    _extForces(i)=0.;
  }
  for (int i=0; i<_nb_impDisp; i++)
  {
    node = _imposeDisplacement[i].node;
    inode = _nb_dim*(node-1)+_imposeDisplacement[i].direction;
    if (!_imposeDisplacement[i].bool_value) //déplacement imposé nul
    {
      if (_vertices[node-1].GetVertice()(1) >= _crackLength+_a || _imposeDisplacement[i].direction==1)
      { //Le noeud est imposé à déplacement nul au-dessus de _a+_crackLength ou imposé nul selon y
        for (Eigen::SparseMatrix<double>::InnerIterator it(_stiffMat,inode); it; ++it)
        {
          it.valueRef() = 1.*(it.row() == inode);
          _stiffMat.coeffRef(inode,it.row()) = 1.*(it.row() == inode);
        }
      }
    }
    else
    {
      for (Eigen::SparseMatrix<double>::InnerIterator it(_stiffMat, inode); it; ++it)
      {
        if (it.row() != inode)
        {
          _extForces(it.row()) -= it.valueRef()*_imposeDisplacement[i].value;
          it.valueRef() = 0.;
          _stiffMat.coeffRef(inode,it.row()) = 0.;
        }
      }
      _extForces(inode) += _imposeDisplacement[i].value;//_move;
      _stiffMat.coeffRef(inode,inode) = 1.;
    }
  }
  for (int i=0; i<_nb_impDisp; i++)
  {
    node = _imposeDisplacement[i].node;
    inode = _nb_dim*(node-1)+_imposeDisplacement[i].direction;
    if (!_imposeDisplacement[i].bool_value) // Imposé nul
    {
      if (_vertices[node-1].GetVertice()(1) >= _crackLength+_a || _imposeDisplacement[i].direction==1)
      {
        _extForces(inode) = 0.;
      }
    }
    else
    {
      _extForces(inode) = _imposeDisplacement[i].value;
    }
  }
  _stiffMat.makeCompressed();
}

//Champs de contraintes élémentaires
Eigen::VectorXd FiniteElements::buildElemStressField(int nb_elem)
{
      //nb_elem : numéro de l'élément (et pas nombre d'éléments !)
      _elemStressField.resize(_nb_dim*3 - 3); //3 en 2D, 6 en 3D
      int npg = _gaussSet.points.size();
      int ti,tj;
      Eigen::MatrixXd deriv, coord, elemStiffMat, Ji, Jii, Ti, Bi, Ci, tBiCi;
      //Jii = inverse de Ji
      Eigen::VectorXd epsiloni, epsilonZero,qe,sigma, sigmaZero, q,epsMeca;
      double detJ;
      Jacobian jacob;
      double weight;
      Jii.resize(_nb_dim,_nb_dim);
      Bi.resize(3,6);
      qe.resize(_nb_dim*(_nb_dim+1)); // 6 en 2D, 12 en 3D
      epsiloni.resize(_nb_dim*3 - 3);
      epsilonZero.resize(_nb_dim*3 - 3);
      epsMeca.resize(_nb_dim*3 - 3);
      sigmaZero.resize(_nb_dim*3 - 3);
      sigma.resize(_nb_dim*3 - 3);
      //extraire qe de q
      //utiliser la table de connectivité
      for (int i=0; i<_nb_dim+1; i++)
      {
          ti = _elements[nb_elem].GetElement()(i)-1; //décalage indices
          for (int k=0;k<_nb_dim;k++)
          {
            qe(_nb_dim*i+k)=_displacement(_nb_dim*ti+k); //vecteur déplacement
          }
      }
      //lire les coordonnées
      coord = coordinates(nb_elem);
      for (int i=0; i<(_nb_dim*3-3); i++)
      {
          sigma(i) = 0.;
      }
      for (int i=0; i<npg; i++)
      {
        //Calcul de Ji
        deriv = interpolationFunctions(_gaussSet.points[i]);
        jacob = jacobian(deriv,coord);
        Ji = jacob.matrix;
        detJ = jacob.det;
        Jii = jacob.inverse;

        //Calcul de Ti
        Ti = Jii*deriv;
        //Redisposition dans Bi
        Bi=buildBi(Ti);

        epsiloni=Bi*qe;
        //lire epsilon zero et sigmaZero -> (pris à zéro)
        for (int k=0;k<(_nb_dim*3-3);k++)
         {
          epsilonZero(k)=0.;
          sigmaZero(k)=0.;
          }
    epsMeca=epsiloni-epsilonZero;
    weight = _gaussSet.weights[i];

    //calcul des contraintes
    sigma+= weight* _elastMat*epsMeca+sigmaZero;
  }
  return sigma;
}

//Champ de contraintes
void FiniteElements::buildStressField()
{
  _stressField.resize(_nb_elements);
  for (int elem=0; elem<_nb_elements; elem++)
  {
    _stressField[elem] = buildElemStressField(elem);
  }
}

//Energie
void FiniteElements::Energy()
{
  Eigen::VectorXd vecteur;
  vecteur=_displacement.transpose()*_oldStiffMat;
  _energy=0.5*vecteur.dot(_displacement);
  std::cout << "Energy = " << _energy << "J" << std::endl;
}


//Résolution du système
void FiniteElements::SolveSystem()
{
  _solver->initialise(_stiffMat, _extForces);
  // _solver->solve();
  _displacement=_solver->getSolution();
  std::cout << "Force nodale 2 = " << (_oldStiffMat*_displacement)(7) << std::endl;
}


//Ecriture de la solution dans les fichiers .vtk
void FiniteElements::WriteSolutions(bool disp_or_stress , int direction)
{
  //disp_or_stress=false si déplacement, true si contraintes
  std::ofstream file_out;
  std::string file_title, file_subtitle, file_end;
  if(! disp_or_stress) //displacement
  {
  switch (direction)
  {
    case 0 :
      file_title="solution_disp_x"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Displacement x";
      break;

    case 1 :
      file_title="solution_disp_y"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Displacement y";
      break;

    case 2 :
      file_title="solution_disp_z"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Displacement z";
      break;

    default :
      std::cout << "The choice of the direction is not possible, plese check the value of the direction." << std::endl;
      exit(0);
  }
  file_end="SCALARS displacement float 1";
}
  else //Stress
  {
  switch (direction)
  {
    case 0 :
      file_title="solution_stress_x"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Stress x";
      break;

    case 1 :
      file_title="solution_stress_y"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Stress y";
      break;

    case 2 :
      if(_nb_dim==2)
      {  file_title="solution_stress_z"+std::to_string(_disconnectedNodes)+".vtk";
        file_subtitle="Stress z";}
      else
      {
        file_title="solution_stress_xy"+std::to_string(_disconnectedNodes)+".vtk";
        file_subtitle="Stress xy";
      }
      break;

    case 3 :
      file_title="solution_stress_zy"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Stress zy";
      break;

    case 4 :
      file_title="solution_stress_zx"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Stress zx";
      break;

    case 5 :
      file_title="solution_stress_xy"+std::to_string(_disconnectedNodes)+".vtk";
      file_subtitle="Stress xy";
      break;

    default :
      std::cout << "The choice of the direction is not possible, please check the value of the direction." << std::endl;
      exit(0);
    }
    file_end="SCALARS stress float 1";
 }
  file_out.open(_resultFolder+file_title, std::ios::out);
  file_out << "# vtk DataFile Version 3.0" << std::endl;
  file_out << file_subtitle << std::endl;
  file_out << "ASCII" << std::endl;
  file_out << "DATASET UNSTRUCTURED_GRID" << std::endl;
  //Ecriture des sommets
  file_out << "Points " << _nb_vert << " float" << std::endl;
  for(int i=0; i<_nb_vert;i++)
  {
    for(int j=0; j<2; j++)
    {
    file_out << _vertices[i].GetVertice()(j)+_displacement(_nb_dim*i+j) << " ";
    }
    file_out << _vertices[i].GetVertice()(2) << " ";
    file_out << std::endl;
  }
  //Ecriture des éléments
  file_out << "CELLS " <<_nb_elements << " " << _nb_elements*(_nb_dim+2) << std::endl;
  for(int i=0; i<_nb_elements; i++)
  {
    file_out << _nb_dim+1 << " ";
    for(int j=0; j<_nb_dim+1; j++)
    {
    file_out << _elements[i].GetElement()(j)-1 << " ";
    }
    file_out << std::endl;
  }

  //Ecriture des types de cellules
  file_out << "CELL_TYPES " << _nb_elements << std::endl;
  for (int i=0; i< _nb_elements; i++)
  {
    file_out << 5*(_nb_dim-1) << std::endl;
  }

  //Ecriture du déplacement
  if(!disp_or_stress) //displacement
  {
    file_out << "POINT_DATA " << _nb_vert << std::endl;
    file_out << file_end << std::endl;
    file_out << "LOOKUP_TABLE default" << std::endl;
    for(int i=0; i< _nb_vert; i++)
    {
      file_out << _displacement(_nb_dim*i+direction)<< std::endl;
    }
  }
  else //stress
  {
    file_out << "CELL_DATA " << _nb_elements << std::endl;
    file_out << file_end << std::endl;
    file_out << "LOOKUP_TABLE default" << std::endl;
    for(int i=0; i< _nb_elements; i++)
    {
      file_out << _stressField[i](direction)<< std::endl;
    }
  }
  file_out.close();
}


//Ecriture du champ de contrainte sur la tranche
void FiniteElements::WriteStressField()
{
  if(_disconnectedNodes!=0) return;
  std::ofstream file_out;
  Eigen::VectorXi element;
  int nb_vert_notch=0;
  bool if_node=false, if_node2=false;
  file_out.open(_resultFolder+"EdgeStress"+std::to_string(_disconnectedNodes)+".txt", std::ios::out);
  for (int elem=0; elem<_nb_elements; elem++)
  {
    element = _elements[elem].GetElement();
    for (auto cn : _imposeDisplacement)
    {
      if (cn.direction==0)
      {
        for(int k=0; k<_nb_dim+1; k++)
        {
          if (cn.node == element(k))
          {
            nb_vert_notch++;
          }
        }
      }
    }
    if (nb_vert_notch == _nb_dim && abs(_elem_center(elem,2) -_b*(_nb_dim-2)/2.) < 1.e-12) //en 3D, on écrit seulement pour la moitié de la profondeur
    {
      file_out << _elem_center(elem,1) << " " << _stressField[elem](0) << std::endl;
    }
    nb_vert_notch=0;
  }
  file_out.close();
}


//Ecriture de l'energie dans un fichier
void FiniteElements::WriteEnergy()
{
  std::string file_name(_resultFolder+"energy_0.txt");
  std::ifstream file_in(file_name.data());
  std::ofstream file_out;
  double W0;
  if (_crackLength == 0.)
  {
    system(("rm -r "+_resultFolder+"energy.txt").c_str());
    file_out.open(_resultFolder+"energy_0.txt");
    file_out << _energy << std::endl;
    file_out.close();
  }
  else
  {
    if (!file_in.is_open())
    {
      std::cout << "--------------------------------------------------" << std::endl;
      std::cout << "Unable to open energy file to read W(0)." << std::endl;
      std::cout << "--------------------------------------------------" << std::endl;
      return;
    }
    file_in >> W0;
    std::cout << "W0 : " << W0 << std::endl;
    std::cout << "W0 - W(a) : " << _energy - W0 << std::endl;
    file_out.open(_resultFolder+"energy.txt", std::ios::app);
    file_out << _crackLength << " " << _energy << " " << -(_energy - W0)/_crackLength << std::endl;
    file_out.close();
  }
}
