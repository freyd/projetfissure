//!\file FiniteElements.h Fichier qui permet d'applilquer la méthode des élements finis au problème
#ifndef H_FINITE_ELEMENTS
#define H_FINITE_ELEMENTS

#include "Mesh.h"
#include "DataFile.h"
#include "Types.h"
#include "Solver.h"
#include <vector>
#include "mpi.h"

//! Une classe Elements Finis
/*!
Cette classe permet de construire un système basé sur un calcul par la méthode des éléments finis.
*/

class FiniteElements
{
protected:
  //Attributs
  //!Pointeur vers le maillage
  Mesh* _mesh;
  //!Solveur
  Solver* _solver;
  //! Nombre de dimensions
  int _nb_dim;
  //!Nombre d'éléments
  int _nb_elements;
  //!Nombre de triangles
  int _nb_tri;
  //!Nombre de sommets
  int _nb_vert;
  //!Nombre d'arêtes de bord,
  int _nb_edg;
  //!Nombre de déplacements imposés
  int _nb_impDisp;
  //! Taille du maillage
  double _meshEp;
  //!Nombre de noeuds déconnectés sur la fissure
  int _disconnectedNodes;
  //!Vecteur contenant les triangles du maillage
  std::vector<Triangle> _triangles;
  //!Vecteur contenant les éléments du maillage
  std::vector<Element> _elements;
  //!Vecteur contenant les centres des éléments du maillage
  Eigen::Matrix<double, Eigen::Dynamic, 3> _elem_center;
  //!Vecteur contenant les sommets du maillage
  std::vector<Vertice> _vertices;
  //!Vecteur contenant les arêtes du bord
  std::vector<Edge> _edges;
  //!Deplacements imposés
  std::vector<ConstrainedNodes> _imposeDisplacement;
  //!Matrice de rigidité globale avant prise en compte des déplacements imposés
  Eigen::SparseMatrix<double> _oldStiffMat;
  //!Matrice de rigidité globale après prise en compte des déplacements imposés
  Eigen::SparseMatrix<double>  _stiffMat;
  //!Vecteur des forces extérieures
  Eigen::VectorXd _extForces;
  //! Champ de contraintes
  std::vector<Eigen::VectorXd> _stressField;
  //! Champ de contraintes élémentaires
  Eigen::VectorXd _elemStressField;
  //!Ensemble des points et poids de Gauss
  GaussSet _gaussSet;
  //!Matrice d'elasticité
  Eigen::MatrixXd _elastMat;
  //!Caractéristique du materiau (module d'Young)
  double _E;
  //!Caractéristique du materiau (coefficient de Poisson)
  double _nu;
  //!Hauteur de l'éprouvette
  double _h;
  //! Hauteur de l'entaille
  double _a;
  //!Profondeur de l'érpouvette
  double _b;
  //! Taille de la fissure
  double _crackLength;
 //!vecteur déplacement
  Eigen::VectorXd _displacement;
  //!valeur de l'énergie
  double _energy;
  //!Nom du dossier résultat
  std::string _resultFolder;
public:
  //! Un constructeur.
  /*!
    Récupère les données géométriques et construit les points de Gauss, initialise les vecteurs.
    \param mesh Un pointeur vers le maillage.
    \param dataFile Un pointeur vers le fichier de données.
    \sa buildGaussSet et computeGeometryQuantities
  */
  FiniteElements(Mesh *mesh, DataFile *dataFile);
  //! Un destructeur par défaut.
  virtual ~FiniteElements();
  //! Construction des quantités géométriques.
  /*!
    Récupère les données du maillage avec Mesh.
  */
  void computeGeometryQuantities();

  //!Fonction des classes filles
  virtual void BuildElasticityMatrix()=0;

  //! Définition des points de Gauss.
  void buildGaussSet(Gauss gaussChoice);

  //! Construction de la matrice de rigidité.
  /*!
    Assemble les matrices de rigidité élémentaires.
    \sa buildElemStiffnessMatrix
  */
  void buildStiffnessMatrix();

  //! Construction d'une matrice de rigidité élémentaire.
  /*!
    \param nb_elem correspond au numéro de l'élément.
    \return La matrice élémentaire de l'élément nb_elem.
    \sa buildStiffnessMatrix
  */
  Eigen::MatrixXd buildElemStiffnessMatrix(int nb_elem);

  //! Construction du vecteur des forces extérieures
  void buildExtForces();

  //!Champ de contraintes élémentaires
  Eigen::VectorXd buildElemStressField(int nb_elem);

  //! Champ de contraintes
  void buildStressField();

  //!Résolution du système
  void SolveSystem();

  //! Renvoie les coordonnées de l'élément nb_elem.
  Eigen::MatrixXd coordinates(int nb_elem);

  //! Fonctions d'interpolations et leurs dérivées.
  /*!
    \param point est le point d'intégration où sont calculées les dérivées.
    \return Une matrice de taille (2,3)
  */
  virtual Eigen::MatrixXd interpolationFunctions(std::tuple<double,double,double> point)=0;

  //! Calcul de la jacobienne
  virtual Jacobian jacobian(Eigen::MatrixXd deriv, Eigen::MatrixXd coord)=0;

//!Construction de Bi à partir des termes de Ti
  virtual Eigen::MatrixXd buildBi(Eigen::MatrixXd Ti) =0;

  //! Accesseur à la matrice de rigidité
  Eigen::SparseMatrix<double> & getStiffMat(){return _stiffMat;};

  //! Accesseur au vecteur des forces extérieures
  Eigen::VectorXd & getExtForces(){return _extForces;};

  //! Accesseur aux points de Gauss.
  GaussSet getGaussSet(){return _gaussSet;};

//!Calcul de l'energie
  void Energy();

  //!Ecriture des solutions dans unfichier parview
  void WriteSolutions(bool disp_or_stress , int direction);

  //!Ecriture de le contrainte x sur le bord de l'arête entaillée
  void WriteStressField();
  //!Ecriture de l'énergie
  void WriteEnergy();

  //!Accesseur à la valeur de l'énergieinterpolationFunctions
  double & getEnergy(){return _energy;};
};

//! Classe fille publique 2D
class Elements2D : public FiniteElements
{
public:
  //!Eléments en 2D
  std::vector<Triangle> _elements;
//! Fonctions d'interpolations et leurs dérivées.
Eigen::MatrixXd interpolationFunctions(std::tuple<double,double,double> point);
//! Calcul de la jacobienne
Jacobian jacobian(Eigen::MatrixXd deriv, Eigen::MatrixXd coord);
//!Construction de D
  void BuildElasticityMatrix();
//!Construction de Bi à partir des termes de Ti
Eigen::MatrixXd buildBi(Eigen::MatrixXd Ti);
//Constructeur de la classe
Elements2D(Mesh* mesh, DataFile* data_file):FiniteElements(mesh, data_file){this -> BuildElasticityMatrix();};

};

//! Classe fille publique 3D
class Elements3D : public FiniteElements
{
public:
  //!Eléments en 3D
  std::vector<Tetrahedra> _elements;
//! Fonctions d'interpolations et leurs dérivées.
Eigen::MatrixXd interpolationFunctions(std::tuple<double,double,double> point);
//! Calcul de la jacobienne
Jacobian jacobian(Eigen::MatrixXd deriv, Eigen::MatrixXd coord);
//!Construction de D
  void BuildElasticityMatrix();
//!Construction de Bi à partir des termes de Ti
Eigen::MatrixXd buildBi(Eigen::MatrixXd Ti);
//!Constructeur de la classe fille
Elements3D(Mesh* mesh, DataFile* data_file):FiniteElements(mesh, data_file){this -> BuildElasticityMatrix();};
};

#endif
