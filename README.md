# Projet CHP : Mise en oeuvre d'un critère d'amorçage de fissure

Ce projet a été réalisé dans le cadre de la formation CHP de l'ENSEIRB-MATMECA, Bordeaux INP.

## Objectif

Le projet a pour but d'obtenir un critère couplé d'amorçage de fissure pour une éprouvette entaillée, à partir d'un critère en contraintes et d'un critère en énergie. Pour cela, une modélisation numérique en C++ a été effectuée, ainsi que des scripts en shell et un script Python pour le post-traitement.

## Description

* Le programme en C++ permet de résoudre les équations de l'élasticité linéaire par la méthode des éléments finis.

* Les scripts en shell permettent une automatisation de la résolution afin de simuler la propagation d'une fissure par incréments.

* Le script Python récupère les données obtenues en sortie de la résolution en C++ pour les manipuler et obtenir un critère d'amorçage de fissure

## Structure du code C++

Pour fonctionner, il est nécessaire d'avoir la bibliothèque Eigen dans ses sources.

* Classe `DataFile` : lecture des paramètres d'entrée
* Classe `Mesh` : contient les caractéristiques du maillage et toutes les méthodes pour le contrôler
* Classe `FiniteElements` : calcul de la matrice du système, du vecteur des forces, des contraintes
* Classe `Solver` : contient les solveurs du système
* Fichiers de sortie : contraintes et déplacements
