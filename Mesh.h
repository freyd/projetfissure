//!\file Mesh.h Fichier qui permet de lire le fichier maillage et de construire les tables géométriques du problème
#ifndef _MESH_H
#include<vector>
#include<string>
#include "Dense"
#include "Sparse"
#include "Types.h"
#include "DataFile.h"
#include "mpi.h"

//!Classe qui permet de définir les sommets en fonctions de leur coordonnées et de leur référence
class Vertice
{
private:
  Eigen::VectorXd _vert; /*!sommet*/
  int _ref; /*!référence du sommet */
public:
  Vertice(Eigen::VectorXd vert, int ref);
  void Print();
  Eigen::VectorXd GetVertice(){return _vert;};
};

//! Classe qui permet de définir les arêtes des bords du maillage en fonction des points et de la référence
class Edge
{
private:
  Eigen::VectorXi _edg; //!points de début et de fin de l'arête
  int _ref; //!référence de l'arête
public:
  Edge(Eigen::VectorXi edg, int ref);
  void Print();
  Eigen::VectorXi GetEdge(){return _edg;};
  int GetRefEdge(){return _ref;};
};

//! Classe permettant de définir les éléments et donnant la table de connectivité
class Element
{
protected:
  Eigen::VectorXi _elem; //! sommets qui constituent l'élément
  int _ref; //! référence de l'élément
public:
  Element(Eigen::VectorXi element, int ref);
  Eigen::VectorXi GetElement(){return _elem;};
};

//! Classe permettant de définir les triangles (éléments 2D)
class Triangle
{
private:
  Eigen::VectorXi _tri; //! sommets qui constituent le triangle
  int _ref; //!référence du triangle
public:
  Triangle(Eigen::VectorXi tri, int ref);
  void Print();
  Eigen::VectorXi GetTriangle(){return _tri;};
};

//! Classe permettant de définir les tétraèdres (éléments 3D)
class Tetrahedra
{
private:
  Eigen::VectorXi _tetra; //! sommets qui constituent le tétraèdre
  int _ref; //!référence du tétraèdre
public:
  Tetrahedra(Eigen::VectorXi tetra, int ref);
  void Print();
  Eigen::VectorXi GetTetrahedra(){return _tetra;};
};

//!Classe qui permet de lire le maillage avec ReadMesh.
/*!
Les structures de données_vertices, _edges, _triangles sont alors créées et peuvent être utilisées
*/
class Mesh
{
protected:
  //!nom du fichier maillage
  std::string _name_mesh;
  //! nombre de dimension du problème
  int  _nb_dim;
  //! sommets
  std::vector<Vertice> _vertices;
  //!Références du maillage
  std::vector<MeshReference> _meshRefs;
  //!sommets ayant un déplacement imposé
  std::vector<ConstrainedNodes> _imposeDisplacement;
  //!triangles
  std::vector<Triangle> _triangles;
  //!elements
  std::vector<Element> _elements;
  //!centre des elements triangles
  Eigen::Matrix<double, Eigen::Dynamic, 3> _tri_center;
  //!centre des elements tétraèdres
  Eigen::Matrix<double, Eigen::Dynamic, 3> _tetra_center;
  //! aretes de bord du domaine
  std::vector<Edge> _edges;

public:
  Mesh(DataFile* data_file);

  void ReadMesh(std::string name_mesh); //!< Fonction qui lit le fichier maillage
  //! Vérification des noeuds dont le déplacement est imposé
  bool ConstrainedNodeCheck(ConstrainedNodes constrainedNode);

  //! Accesseur à la matrice des sommets
  std::vector<Vertice> GetVertices(){return _vertices;};
  //! Accesseur à la table des Triangles
  std::vector<Triangle> GetTriangles(){return _triangles;};
  //! Accesseur à la table de connectivité
  std::vector<Element> GetElements(){return _elements;};

  //! Accesseur aux centres des triangles
  virtual Eigen::Matrix<double, Eigen::Dynamic, 3> GetElementCenter()=0;
  //!Accesseur aux arêtes
  std::vector<Edge> GetEdges(){return _edges;};
  //! Accesseur aux déplacements imposés
  std::vector<ConstrainedNodes> GetImposeDisplacement(){return _imposeDisplacement;};

  //!Fonctions des classes filles
  virtual void BuildElementsCenter()=0; //!< Fonction qui construit le centre et l'aire des éléments
  virtual void ReadElements(std::ifstream& mesh_file, std::string file_line, int me)=0;//!< Fonction qui lit les éléments

};

//! Classe fille publique 2D
class Mesh2D : public Mesh
{
public:
  void BuildElementsCenter(); //!< Fonction qui construit le centre et l'aire des éléments
  Eigen::Matrix<double, Eigen::Dynamic, 3> GetElementCenter(){return _tri_center;};
  //! Fonction permettant de lire les éléments 2D
  void ReadElements(std::ifstream& mesh_file, std::string file_line, int me);

  //!Constructeur
  Mesh2D(DataFile* data_file):Mesh(data_file){_nb_dim=2;};
};
//!Classe fille publique 3D
class Mesh3D : public Mesh
{
public:
  //!Fonction qui trouve le centre des éléments
  void BuildElementsCenter();
  Eigen::Matrix<double, Eigen::Dynamic, 3> GetElementCenter(){return _tetra_center;};
  //! Fonction permettant de lire les triangles et les éléments 3D
  void ReadElements(std::ifstream& mesh_file, std::string file_line, int me);
  //!Constructeur
  Mesh3D(DataFile* data_file):Mesh(data_file){_nb_dim=3;};
};


#define _MESH_H
#endif
