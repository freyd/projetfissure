//!\file Solver.h Fichier qui permet de résoudre le système linéaire
#ifndef H_SOLVER
#define H_SOLVER

#include "Dense"
#include "Sparse"

//!Classe abstraite de solveurs linéaires.
/*!
  Possède actuellement deux classes filles de solveurs de la bibliothèque <A HREF="http://eigen.tuxfamily.org">Eigen</A>.
*/
class Solver
{
protected:
  //! Matrice du système.
  Eigen::SparseMatrix<double> _A;
  //! Second membre.
  Eigen::VectorXd _b;
  //! Vecteur solution.
  Eigen::VectorXd _x;
public:
  //! Constructeur par défaut.
  Solver();
  //! Destructeur par défaut
  virtual ~Solver(){};
  //! Initialisation et résolution du système
  /*!
    \param A Matrice du système.
    \param b Second membre du système.
  */
  virtual void initialise(Eigen::SparseMatrix<double> A, Eigen::VectorXd b) = 0;
  //! Accesseur de la solution
  Eigen::VectorXd getSolution(){return _x;};
};

//! Classe héritée de Solver
/*!
  Utilise le solveur SimplicialLDLT de Eigen
*/
class SolverLDLT : public Solver
{
private:
  // Objet de la classe SimplicialLDLT
  Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>>* _solverLDLT;
public:
  //! Constructeur
  SolverLDLT();
  //Destructeur
  ~SolverLDLT();
  //! Initialisation et résolution
  /*!
    \param A Matrice du système.
    \param b Second membre du système.
  */
  void initialise(const Eigen::SparseMatrix<double> A, Eigen::VectorXd b);
};


//! Classe héritée de Solver
/*!
  Utilise le solveur ConjugateGradient de Eigen
*/
class SolverCG : public Solver
{
private:
  // Objet de la classe SimplicialLDLT
  Eigen::ConjugateGradient<Eigen::SparseMatrix<double>>* _solverCG;
public:
  //! Constructeur
  SolverCG();
  //Destructeur
  ~SolverCG();
  //! Initialisation et résolution
  /*!
    \param A Matrice du système.
    \param b Second membre du système.
  */
  void initialise(const Eigen::SparseMatrix<double> A, Eigen::VectorXd b);
};

#endif
