import matplotlib
# matplotlib.use('agg')
import numpy as np
import pylab
import matplotlib.pyplot as plt
import sys
from scipy import interpolate
import os.path
import os
from subprocess import call

## ----------------------------- Get the folder ----------------------------- ##

pwd = os.path.relpath(".")
folder = "/2D/Fissure_parallele/Dunn_First_half/"
# folder = "/2D/Fissure_parallele/Dunn_First_test/"
# folder = "/2D/Fissure_parallele/Dunn_First_ref/"
# folder = "/2D/Fissure_parallele/Dunn_Second_test/"
# folder = "/TestPlafrim/Dunn_raff/"
os.system("mkdir -p "+pwd+folder+"/PostProcess")


## ------------------------------- Parameters ------------------------------- ##

# Young's modulus
E = 2.3e+3
# Height
h = 17.8
# Imposed displacement
d = 0.1
# Length of the notch
a = 1.78
# Lc/h
LcH = 0.0033


## ------------------------- Recovering of the data ------------------------- ##

sigma = []
G = []

sigma = pylab.loadtxt(pwd+folder+"EdgeStress0.txt")
G = pylab.loadtxt(pwd+folder+"energy.txt")


## ------------------------ Calculation of A and kxx ------------------------ ##

n_kxx = sorted(sigma, key=lambda tup : tup[0])
# print(n_kxx)

kxx_0 = [n_kxx[i][0]-a for i in range(len(n_kxx))]
kxx_1 = [n_kxx[i][1]/(E*(d/h)) for i in range(len(n_kxx))]
kxx = [kxx_0,kxx_1]
# print(kxx[0])

A_0 = G[:,0]
A_1 = G[:,2]/(E*h*(d/h)**2)
A = [A_0,A_1]
# print(A[0])


## ----------------------------- Interpolation ------------------------------ ##

Ainterp = interpolate.interp1d(A[0],A[1])
kxxinterp = interpolate.interp1d(kxx[0],kxx[1])

def Akxx(y):
    """Return the value of A(y)/kxx(y)**2"""
    return Ainterp(y)/(kxxinterp(y)**2)


## ---------------------------------- Plot ---------------------------------- ##

x_min = max(A[0][0],kxx[0][0])
x_max = min(A[0][-1],kxx[0][-1])
x = np.linspace(x_min,x_max,100)
y = Akxx(x)
sigc = 500.
d = []
itr = []

for i in range(len(x)):
    try:
        d += [sigc/E * np.sqrt(y[i])/np.sqrt(Ainterp(x[i]))]
        # d += [sigc/E * np.sqrt(y[i])/np.sqrt(Ainterp(x[i]+a))]
    except ValueError:
        # d += [0.]
        itr += [i]

n_y = np.delete(y,itr)
n_x = np.delete(x,itr)

n_y = n_y
n_x = n_x


## ----------------------- A/kxx**2 en fonction de a ------------------------ ##

plt.plot(x,y)
plt.xlabel(("a (mm) incremental crack").decode('utf-8'))
plt.ylabel("A/kxx**2")
plt.title(("A/kxx**2 function of the length a").decode("utf-8"))
plt.savefig(pwd+folder+"PostProcess/Akxx2_a.png")
plt.show()
plt.close()

## ------------------------- a* en fonction de Lc/h ------------------------- ##

plt.plot(y,x)
plt.ylabel(("a* (mm) crack length").decode('utf-8'))
plt.xlabel("Lc/h")
plt.title(("Crack length a* function of Lc/h").decode("utf-8"))
plt.savefig(pwd+folder+"PostProcess/a_Lch.png")
plt.show()
plt.close()

## -------------- d* en fonction de Lc/h pour differents sig_C -------------- ##

sig = [104.,124.,144.]
# sig = np.linspace(0,125,11)
axe_y = np.linspace(0,0.5,20)
for sigc in sig:
    dk = sigc/E * np.sqrt(n_y)*h/np.sqrt(Ainterp(n_x))
    plt.plot(n_y,dk,label=str(sigc))
plt.plot([LcH for i in range(20)],axe_y,'+-')

plt.ylabel(("d* (mm) critical load").decode('utf-8'))
plt.xlabel("Lc/h")
plt.title(("Critical load d* function of Lc/h").decode("utf-8"))
plt.legend(loc=2,title="Sigma C")
plt.savefig(pwd+folder+"PostProcess/d_Lch.png")
plt.show()
plt.close()
