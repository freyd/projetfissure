//!\file Types.h Fichier qui permet de d"finir les différents types utilisés dans le programme
#ifndef H_TYPES
#define H_TYPES

#include "Dense"
#include "Sparse"
#include <map>
#include <string>

//! Un type struct pour les points de Gauss.
struct GaussSet
{
  std::vector<std::tuple<double,double,double>> points; /*!< Points d'intégrations.*/
  std::vector<double> weights; /*!< Poids.*/
};


//! Un type struct pour les matrices Ji, leurs inverses et leur déterminant
struct Jacobian
{
  Eigen::MatrixXd matrix; /*!< Matrice */
  double det; /*!< Déterminant */
  Eigen::MatrixXd inverse; /*!< Inverse */
};


//! Un type struct pour les noeuds ayant des déplacements imposés
struct ConstrainedNodes
{
  int node; //!< Sommet concerné par le déplacement imposé
  bool bool_value; //!< Booléen sur le caractère non-zéro du déplacement imposé
  double value; //!< Valeur numérique du déplacement imposé
  int direction; //!< Direction du déplacement imposé
};

//! Un struct pour les références du maillage.
struct MeshReference
{
  int ref; //!< Reference
  // bool edge; //!< True si une edge, False si un vertice
  int dim; //!< 0 si un vertice, 1 si une edge, 2 si un triangle
  double value; //!< Valeur numérique du déplacement
  int direction; //!< 0 sur x et 1 sur y
};


// Un type enum pour les choix de la règle de Gauss
enum Gauss
{
  Gauss2d4=0, Gauss2d5=1, Gauss3d4=2, Gauss3d5=3, Gauss3d6=4
};


// Un type enum pour le choix du solveur
enum SOLVER
{
  solvLDLT=0, solvCG=1
};


#endif
