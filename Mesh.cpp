#ifndef _MESH_CPP

#include "Mesh.h"
#include<iostream>
#include<fstream>
#include <assert.h>

using namespace std;

Vertice::Vertice(Eigen::VectorXd vert, int ref) : _vert(vert), _ref(ref)
{
  assert((_vert.size() == 3) && "The size of the point is not equal to 3 !");
}

void Vertice::Print()
{
  cout << "[x, y, z] = [" << _vert[0] << " " << _vert[1] << _vert[2] << "];" << endl;
  cout << "ref = " << _ref << endl;
}


Edge::Edge(Eigen::VectorXi edg, int ref) : _edg(edg), _ref(ref)
{
  if (_edg[0] > _edg[1])
  {
    int a(_edg[0]);
    _edg[0] = _edg[1];
    _edg[1] = a;
  }
  assert((_edg.size() == 2) && "The size of the edge is not equal to 2 !");
}

void Edge::Print()
{
  cout << "[pt1, pt2] = [" << _edg[0] << " " << _edg[1] << "];" << endl;
  cout << "ref = " << _ref << endl;
}

Triangle::Triangle(Eigen::VectorXi tri, int ref) : _tri(tri), _ref(ref)
{
  assert((_tri.size() == 3) && "The size of the triangle is not equal to 3 !");
}

void Triangle::Print()
{
  cout << "[pt1, pt2, pt3] = [" << _tri[0] << " " << _tri[1] << " " << _tri[2] << "];" << endl;
  cout << "ref = " << _ref << endl;
}

Tetrahedra::Tetrahedra(Eigen::VectorXi tetra, int ref) : _tetra(tetra), _ref(ref)
{
  assert((_tetra.size() == 4) && "The size of the tetrahedra is not equal to 4 !");
}

void Tetrahedra::Print()
{
  cout << "[pt1, pt2, pt3, pt4] = [" << _tetra[0] << " " << _tetra[1] << " " << _tetra[2] << " " << _tetra[3] <<"];" << endl;
  cout << "ref = " << _ref << endl;
}

Element::Element(Eigen::VectorXi elem, int ref) : _elem(elem), _ref(ref)
{
  assert((_elem.size() == 4) && "The size of the element is not equal to 4 !");
}

Mesh::Mesh(DataFile* data_file)
{
  _meshRefs = data_file->getMeshReferences();
}

void Mesh2D::BuildElementsCenter()
{
  _tri_center.resize(_elements.size(),3);
  vector<int> p(3);
  //p contient la valeur du sommet dans le maillage des trois sommets
  vector<double> x(3), y(3);
  //x (y) contient la première (deuxième) coordonnée des trois sommets du triangle
  for (int i = 0; i < _elements.size(); i++) //On parcourt chaque triangle
  {
    for (int j = 0; j < 3; j++) //On récupère les trois points du triangle
    {
      // Récupère les points du triangle
      p[j] = _elements[i].GetElement()[j]; //a changer élément
      // Récupère la coordonnée x des points avec _vertices qui commence à 0
      x[j] = _vertices[p[j]-1].GetVertice()[0];
      // Et la coordonnée y
      y[j] = _vertices[p[j]-1].GetVertice()[1];
    }
    //Construire la matrice contenant les coordonnées des centres de chaque triangle
    //Calcul des coordonnées du centre du triangle
    _tri_center(i,0)= (x[0]+x[1]+x[2])/3.;
    _tri_center(i,1)= (y[0]+y[1]+y[2])/3.;
    _tri_center(i,2)= 0.;
  }
}

void Mesh3D::BuildElementsCenter()
{
  _tetra_center.resize(_elements.size(),3);
  vector<int> p(4);
  //p contient la valeur du sommet dans le maillage des quatre sommets
  vector<double> x(4), y(4), z(4);
  //x (y) contient la première (deuxième) coordonnée des quatre sommets du tétrahèdre
  for (int i = 0; i < _elements.size(); i++) //On parcourt chaque élément
  {
    for (int j = 0; j < 4; j++) //On récupère les quatre points du tétrahedre
    {
      // Récupère les points du tétraèdre
      p[j] = _elements[i].GetElement()[j];
      // Récupère la coordonnée x des points avec _vertices qui commence à 0
      x[j] = _vertices[p[j]-1].GetVertice()[0];
      // La coordonnée y
      y[j] = _vertices[p[j]-1].GetVertice()[1];
      // Et la coordonnée z
      z[j] = _vertices[p[j]-1].GetVertice()[2];
    }
    //Construire la matrice contenant les coordonnées des centres de chaque triangle
    //Calcul des coordonnées du centre du triangle
    _tetra_center(i,0)= (x[0]+x[1]+x[2]+x[3])/4.;
    _tetra_center(i,1)= (y[0]+y[1]+y[2]+y[3])/4.;
    _tetra_center(i,2)= (z[0]+z[1]+z[2]+z[3])/4.;
  }
}

bool Mesh::ConstrainedNodeCheck(ConstrainedNodes constrainedNode)
{
  for (auto cn : _imposeDisplacement)
  {
    if (cn.node == constrainedNode.node && cn.value == constrainedNode.value && cn.direction == constrainedNode.direction)
    {
      return false;
    }
    if (cn.node == constrainedNode.node && cn.direction == constrainedNode.direction && cn.value != constrainedNode.value)
    {
      std::cout << "Problem in mesh references. Program ending." << std::endl;
      exit(0);
    }
  }
  return true;
}


void Mesh::ReadMesh(string name_mesh)
{
  _name_mesh = name_mesh;
  ConstrainedNodes constrainednodes;
  ifstream mesh_file(name_mesh.data());
  int me;
  MPI_Comm_rank(MPI_COMM_WORLD,&me);
  if (!mesh_file.is_open())
  {
    if(me==0) cout << "Unable to open file " << name_mesh << endl;
    abort();
  }
  else
  {
    if(me==0)  cout << "-------------------------------------------------" << endl;
    if(me==0)  cout << "Reading mesh: " << name_mesh << endl;
  }

  string file_line;
  int nb_dim;
  int nb_vertices(0), nb_triangles(0), nb_edges(0);
  Eigen::VectorXd vert(3);
  double z(0);
  Eigen::VectorXi tri(3), edg(2);
  int ref(0), loop_vertices(1);

  while (!mesh_file.eof())
  {
    getline(mesh_file, file_line);
    if ((file_line.find("Vertices") != std::string::npos)&&(loop_vertices))
    {
      mesh_file >> nb_vertices;
      if(me==0)  cout << "Number of vertices  (" << nb_vertices << ")" << endl;
      for (int i = 0 ; i < nb_vertices ; ++i)
      {
        mesh_file >> vert[0] >> vert[1] >> vert[2] >> ref;
        Vertice vertice(vert, ref);

        for (auto meshref : _meshRefs)
        {
          if (meshref.dim == 0 && ref == meshref.ref)
          {
            constrainednodes.node = i+1;
            constrainednodes.value = meshref.value;
            constrainednodes.bool_value = (meshref.value != 0.);
            constrainednodes.direction = meshref.direction;
            if (ConstrainedNodeCheck(constrainednodes))
            {
              _imposeDisplacement.push_back(constrainednodes);
            }
          }
        }
        _vertices.push_back(vertice);
      }
      if(me==0)  assert((_vertices.size() == nb_vertices) && "Problem 1 in mesh reading.");
      loop_vertices = 0;
    }
    if (file_line.find("Edges") != std::string::npos)
    {
      mesh_file >> nb_edges;
      if(me==0)  cout << "Number of edges (" << nb_edges << ")" << endl;
      for (int i = 0 ; i < nb_edges ; ++i)
      {
        mesh_file >> edg[0] >> edg[1] >> ref;
        Edge edge(edg, ref);

        for (auto meshref : _meshRefs)
        {
          if (meshref.dim == 1 && ref == meshref.ref)
          {
            constrainednodes.node = edg[0];
            constrainednodes.value = meshref.value;
            constrainednodes.bool_value = (meshref.value != 0.);
            constrainednodes.direction = meshref.direction;
            if (ConstrainedNodeCheck(constrainednodes))
            {
              _imposeDisplacement.push_back(constrainednodes);
            }
            constrainednodes.node = edg[1];
            if (ConstrainedNodeCheck(constrainednodes))
            {
              _imposeDisplacement.push_back(constrainednodes);
            }
          }
        }
        _edges.push_back(edge);
      }
      if(me==0)  assert((_edges.size() == nb_edges) && "Problem 2 in mesh reading.");
    }
    ReadElements(mesh_file, file_line, me);
  }
  if(_elements.size()==0)
  {
    if(me==0)  std::cout << "Problem in the mesh dimension, please check your datafile."<< std::endl;
    if(me==0)  std::cout << "Program ending" << std::endl;
    exit(0);
  }

  if(me==0)  cout << "Building triangles center" << endl;
  BuildElementsCenter();
  if(me==0)  cout << "End Reading Mesh " << endl;
  if(me==0)  cout << "-------------------------------------------------" << endl;
}

void Mesh2D::ReadElements(ifstream& mesh_file, string file_line, int me)
{
  int nb_triangles(0);
  Eigen::VectorXd vert(3);
  double z(0);
  Eigen::VectorXi tri(4), edg(2);
  int ref(0), loop_vertices(1);

  if (file_line.find("Triangles") != std::string::npos)
  {
    mesh_file >> nb_triangles;
    if(me==0)  cout << "Number of triangles (" << nb_triangles << ")" << endl;
    for (int i = 0 ; i < nb_triangles ; ++i)
    {
      mesh_file >> tri[0] >> tri[1] >> tri[2] >> ref;
      tri[3]=0;
      Element element(tri, ref);
      _elements.push_back(element);
    }
    if(me==0)  assert((_elements.size() == nb_triangles) && "Problem in mesh reading.");
  }
}

void Mesh3D::ReadElements(ifstream& mesh_file, string file_line, int me)
{
  ConstrainedNodes constrainednodes;
  int nb_vertices(0), nb_triangles(0), nb_tetrahedra(0);
  Eigen::VectorXd vert(3);
  double z(0);
  Eigen::VectorXi tri(3), edg(2), tetra(4);
  int ref(0), loop_vertices(1);

  if (file_line.find("Triangles") != std::string::npos)
  {
    mesh_file >> nb_triangles;
    if(me==0)  cout << "Number of triangles (" << nb_triangles << ")" << endl;
    for (int i = 0 ; i < nb_triangles ; ++i)
    {
      mesh_file >> tri[0] >> tri[1] >> tri[2] >> ref;
      Triangle triangle(tri, ref);
      for (auto meshref : _meshRefs)
      {
        if (meshref.dim == 2 && ref == meshref.ref)
        {
          constrainednodes.node = tri[0];
          constrainednodes.value = meshref.value;
          constrainednodes.bool_value = (meshref.value != 0.);
          constrainednodes.direction = meshref.direction;
          if (ConstrainedNodeCheck(constrainednodes))
          {
            _imposeDisplacement.push_back(constrainednodes);
          }
          constrainednodes.node = tri[1];
          if (ConstrainedNodeCheck(constrainednodes))
          {
            _imposeDisplacement.push_back(constrainednodes);
          }
          constrainednodes.node = tri[2];
          if (ConstrainedNodeCheck(constrainednodes))
          {
            _imposeDisplacement.push_back(constrainednodes);
          }
        }
      }
      _triangles.push_back(triangle);
    }
    if(me==0)  assert((_triangles.size() == nb_triangles) && "Problem 3 in mesh reading.");
  }
  if (file_line.find("Tetrahedra") != std::string::npos)
  {
    mesh_file >> nb_tetrahedra;
    if(me==0)  cout << "Number of tetrahedra (" << nb_tetrahedra << ")" << endl;
    for (int i = 0 ; i < nb_tetrahedra ; ++i)
    {
      mesh_file >> tetra[0] >> tetra[1] >> tetra[2] >> tetra[3] >> ref;
      Element element(tetra, ref);
      _elements.push_back(element);
    }
    if(me==0)  assert((_elements.size() == nb_tetrahedra) && "Problem 3 in mesh reading.");
  }
}


#define _MESH_CPP
#endif
