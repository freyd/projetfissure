//! \file Tools.h Fichier contenant la fonction charge pour le parallélisme
#include <utility>

std::pair<int,int> Charge(int me, int nb_proc, int nb_op);
